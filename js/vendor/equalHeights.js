(function($) {

  // equalHeights - compare two or more elements and animate them all to the
  // same (min-)height.
  // You'll need jQuery for this to work.
  $.fn.equalHeight = function() {
    var heights = [];
    var height;
    $(this).each(function() {
      // Cache the current height.
      height = $(this).css('min-height');
      // Reset min-height (in case it was already set).
      $(this).css({'min-height': ''});
      // Get the element's real height.
      heights.push($(this).outerHeight());
      // Give it back what it had.
      $(this).css({'min-height': height});
    });
    // Animate all of them to the highest element's height and add a class to
    // tell us it was altered, this class can be used to revert this effect on
    // certain media queries like this:
    //  .equal-height {
    //    min-height: 0 !important;
    //  }
    $(this).animate({'min-height': Math.max.apply(Math, heights)}, 200).addClass('equal-height');
  };



  // You can use something like this to trigger 'equalHeight' after ajax calls.
  // $(document).ajaxComplete(function() {
  //   $(document).trigger('equalHeight');
  // });

})(jQuery);
