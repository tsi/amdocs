// CSS Feature detection
// http://ryanmorr.com/detecting-css-style-support/
(function(win){
    'use strict';

    var el = win.document.createElement('div'),
    camelRe = /-([a-z]|[0-9])/ig,
    cache = {},
    support,
    camel,
    key;

    win.isStyleSupported = function(style, value){
        value = arguments.length === 2 ? value : 'inherit';
        if('CSS' in win && 'supports' in win.CSS){
            return win.CSS.supports(style, value);
        }
        if('supportsCSS' in win){
            return win.supportsCSS(style, value);
        }
        key = style + ':' + value;
        if(key in cache){
            return cache[key];
        }
        support = false;
        camel = style.replace(camelRe, function(all, letter){
            return (letter + '').toUpperCase();
        });
        support = (typeof el.style[camel] === 'string');
        el.style.cssText = style+':'+value;
        support = support && (el.style[camel] !== '');
        return cache[key] = support;
    };

})(this);

$(document).ready(function(){

  // Development file watcher - REMOVE FROM PRODUCTION.
  var isWebkit = 'WebkitAppearance' in document.documentElement.style
  if (location.host == 'amdocs.lh' && isWebkit) {
    $(document).watch('/css/base.css');
    $(document).watch('/css/components.css');
  }

  // Resize handler
  function resizeHandler() {
    if (window.matchMedia("(min-width: 641px)").matches) {
      $('html')
        .removeClass('tablet-disabled')
        .addClass('tablet');
    }
    else {
      $('html')
        .removeClass('tablet')
        .addClass('tablet-disabled');
    }
  };
  if ($('html').is('.tablet')) {
    var resizeTimer;
    $(window).resize(function() {
      clearTimeout(resizeTimer);
      resizeTimer = setTimeout(resizeHandler, 100);
    });
  }

  // Flexbox Support
  if(isStyleSupported('display', 'flex')){
    // Function seems to mistake - it says that IE9 supports FLEX while it's not, so I added this validation.
    if( ! $('html').hasClass('ie9')){
      document.documentElement.className += ' flex-yep';
    }else {
      document.documentElement.className += ' flex-nope';
    }
  }
  else {
    document.documentElement.className += ' flex-nope';
    // Fix for IE8&9 in case that flex is not supported
    $(document).bind('equalHeight', function() {
      $('.main-post').equalHeight();
    });
    $(window).load(function() {
     $(document).trigger('equalHeight');
    });
  }

  // check JS support
  $('html').removeClass('no-js');

  // Responsive Tables
  $('.responsive-table', '.main-content').stackTableRows();

  // Responsive Tabs
  $('.responsive-tabs').easyResponsiveTabs( {closed: 'accordion'} );

});

var Menus = (function() {

  $(document).ready(function() {

    // Active menu items
    var currentLocation = $('body').data('path'),
        activeMainMenuLink = currentLocation ? $('.nav-main > li a[href="' + currentLocation + '"]') : false;
        activeMobileMenuLink = currentLocation ? $('.with-sub-menu > a[href="' + currentLocation + '"]', '.mobile-nav-container') : false;
    // Active main menu items
    if (activeMainMenuLink.length) {
      activeMainMenuLink.addClass('active');
    }
    // Mobile menu - open current level on load
    if (activeMobileMenuLink.length) {
      activeMobileMenuLink.click();
    }

    var showSideMenu = function() {
      if (activeMobileMenuLink.length) {
        activeMobileMenuLink.click();
      }
      $('.mobile-nav-container').addClass('visible');
      $('#page').addClass('side-menu');
      if ($('html').is('.ie9')) {
        $('#page').animate({
          left: "284px"
        }, 300 );
      }
    }

    var hideSideMenu = function() {
      $('.mobile-nav-container').removeClass('visible');
      $('#page').removeClass('side-menu');
      if ($('html').is('.ie9')) {
        $('#page').animate({
          left: "0"
        }, 300 );
      }
      setTimeout(function() {
        $('.mobile-nav-sub-menu').removeClass('visible');
        $('ul.main-menu').removeClass('sub-menu-opened');
      }, 1000);
    }

    var toggleSideMenu = function() {
      if ($('#page').is('.side-menu')) {
        hideSideMenu();
      }
      else {
        showSideMenu();
      }
    }

    // Trigger Slide Menu.
    $('.mobile-menu-icon').on('click',function(e){
      e.preventDefault();
      toggleSideMenu();
    });

    // Language icon
    $('a.mobile-lang-icon').on('click',function(e) {
      e.preventDefault();
      if ($('.lang > .visible', '.mobile-nav-container').length) {
        hideSideMenu();
      }
      else {
        showSideMenu();
        $('.mobile-nav-sub-menu').removeClass('visible');
        $('.lang.with-sub-menu', '.mobile-nav-container')
          .children('.mobile-nav-sub-menu')
          .addClass('visible')
          .closest('ul.main-menu')
          .addClass('sub-menu-opened');
      }
    });

    // Trigger Search box.
    $('.mobile-search-icon').on('click',function(e) {
      e.preventDefault();
      $('.nav-main-btn').toggleClass('visible');
    });

    // Mobile side-menu.
    $('.with-sub-menu > a', '.mobile-nav-container').on('click',function(e) {
      e.preventDefault();
      $(this)
        .parents('.sub-menu-wrp')
        .find('.mobile-nav-sub-menu')
        // .removeClass('visible');
      $(this)
        .siblings('.mobile-nav-sub-menu')
        .addClass('visible')
        .parents('.sub-menu-wrp')
        .addClass('sub-menu-opened visible');
    });

    $('.back', '.mobile-nav-container').on('click',function(e) {
      e.preventDefault();
      var that = $(this);
      that.closest('.sub-menu-wrp.sub-menu-opened').removeClass('sub-menu-opened');
      setTimeout(function() {
        that.closest('.mobile-nav-sub-menu').removeClass('visible')
      }, 1000);
    });

    //language drop-down menu
    $(".globe-icon").on("click",function(e){
      e.preventDefault();
      $(this).toggleClass("active");
      $(".lang-dd-menu").toggleClass("active");
    });

    $('.lang-dd-menu ul a').on('click',function(e) {
      e.preventDefault();
      $(this).removeClass("active");
      $(".lang-dd-menu").removeClass("active");
      $(".globe-icon").removeClass("active");
      $('.lang .chosen-lang').text($(this).text());
    });

    $(document).click(function(e) {
      if(!$(e.target).hasClass('globe-icon')){
        $(".lang-dd-menu").removeClass("active");
        $(".globe-icon").removeClass("active");
      }
    });

    $('.lang ul a', '.mobile-nav-container').on('click',function(e) {
      e.preventDefault();
      $('.lang ul a').removeClass('active');
      $(this).addClass('active');
      $('.lang .chosen-lang').text($(this).text());
      $(this).closest('ul.main-menu').removeClass('sub-menu-opened');
    });

    $('.with-dd-menu', '.nav-container.left-nav-container').on({
      mouseenter: function(){
        $('.with-dd-menu')
          .not(this)
          .children('a')
          .removeClass('active')
          .siblings('.nav-dd-menu')
          .stop()
          .slideUp();
        $(this)
          .children('.nav-dd-menu')
          .stop()
          .slideDown()
          .siblings('a')
          .addClass('active');
      }
    });

    $('.with-dd-menu > a', '.nav-container.mobile-nav-container').on('click',function(e) {
      e.preventDefault();
      $('.with-dd-menu > a').not(this).removeClass('active').siblings('.nav-dd-menu').slideUp();
      $(this).toggleClass('active').siblings('.nav-dd-menu').slideToggle();
    });

    // Desktop menu - prevent mouseleave if leaving the window
    var inWindow = true;
    $('body').hover(
      function() {
        inWindow = true;
        $('.nav-main > li.hovered:not(:hover)').removeClass('hovered');
      },
      function() {
        inWindow = false;
      }
    );
    $('.nav-main > li').hover(
      function() {
        $(this).addClass('hovered');
      },
      function() {
        setTimeout(function () {
          if (inWindow == true) {
            $('.nav-main > li.hovered:not(:hover)').removeClass('hovered');
          }
        }, 200);
      }
    );
    $('.nav-main > li').click(function() {
      $(this).toggleClass('hovered');
    });

  });

}());


var Slider = (function() {

  $(document).ready(function() {

    // Slick Slider Plugin
    $('.slider').slick({
      onBeforeChange: toggleNavItem,
      onAfterChange: toggleNavItem,
      autoplay: true,
      autoplaySpeed: 3000,
      arrows: false
    });

    // Bind arrow next/prev click events
    $(".slider").find(".slider-next").on("click",function(e){
        e.preventDefault();
       $(".slick-slider").slickNext();
    });
    $(".slider").find(".slider-prev").on("click",function(e){
       e.preventDefault();
       $(".slick-slider").slickPrev();
    });

    // Create nav items.
    $('.slick-slide').not(".slick-cloned").each(function() {
      $('<a href="#" class="slider-nav-item"></a>')
        .insertAfter('.slider-prev')
        .click(function(e) {
          // Slider nav click handler.
          e.preventDefault();
          $(".slick-slider").slickGoTo( $(this).index(".slider-nav-item"));
        });
    });

    function toggleNavItem(){
      var navItemIdx = $('.slick-slide').not(".slick-cloned").index($(".slick-active"));
      var navItem =  $(".slider-nav-item").eq(navItemIdx);
      $(".slider-nav-item").removeClass("active");
      navItem.toggleClass('active');
    }

    // Init first nav item
    $(".slider-nav-item").eq(0).toggleClass('active');

  });

}());

var VerticalSlider = (function(){

  $(document).ready(function(){
        $('.vertical-slider').slick({
          slidesToShow: 3,
          slidesToScroll: 1,
          autoplay: true,
          autoplaySpeed: 2000,
          vertical: true,
          arrows: false
        });
  });

    // Bind arrow next/prev click events
    $(".vertical-slider").find(".vertical-slider-next").on("click",function(e){
        e.preventDefault();
       $(".vertical-slider").slickNext();
    });
    $(".vertical-slider").find(".vertical-slider-prev").on("click",function(e){
       e.preventDefault();
       $(".vertical-slider").slickPrev();
    });

}());

var Form = (function() {

  $(document).ready(function(){
    // Init Chosen.js
    $(".chosen-select").chosen({width: "100%"});
    // Cancel iOs deafult input zoom
    $('select').cancelZoom();
    $('input').cancelZoom();
    $('textarea').cancelZoom();
  })
  // Func for disabling iOs default input zoom
  function cancelZoom(){
      var d = document,
          viewport,
          content,
          maxScale = ',maximum-scale=',
          maxScaleRegex = /,*maximum\-scale\=\d*\.*\d*/;

      // this should be a focusable DOM Element
      if(!this.addEventListener || !d.querySelector) {
          return;
      }

      viewport = d.querySelector('meta[name="viewport"]');
      content = viewport.content;

      function changeViewport(event)
      {
          // http://nerd.vasilis.nl/prevent-ios-from-zooming-onfocus/
          viewport.content = content + (event.type == 'blur' ? (content.match(maxScaleRegex, '') ? '' : maxScale + 10) : maxScale + 1);
      }

      // We could use DOMFocusIn here, but it's deprecated.
      this.addEventListener('focus', changeViewport, true);
      this.addEventListener('blur', changeViewport, false);
  }

  // jQuery-plugin
  (function($) {
    $.fn.cancelZoom = function() {
      return this.each(cancelZoom);
    };
  })(jQuery);



}());

var Subscribe = (function() {
  $(".subscriber-select").chosen({
    "width": "100%",
    "disable_search": true
  });
}());

var Videos = (function() {

  $.fn.loadVideo = function(options) {
    var defaults = {
          videoID: $(this).attr('data-video-id'),
          targetID: 'vidplayer',
          height: '315',
          width: '560'
        },
        opt = $.extend({}, defaults, options);

    $('iframe.' + opt.targetID).attr('src', 'http://www.youtube.com/embed/' + opt.videoID + '?autohide=1&autoplay=0&showinfo=0&wmode=transparent&theme=light');
  }

  $(document).ready(function() {
    $('[data-video-id]', '.video-gallery').on('click', function(e) {
      e.preventDefault();
      $(this).loadVideo();
    });
  });

}());


var Lightbox =  (function(){

  $(document).ready(function(){

    $(".lgt-box[data-video-id]").on('click',function(e){

      e.preventDefault();

      // Create Lightbox Elements on video click;
      var lbox = {
        bg : $("<div>", {'class': "lbox-bg"}),
        wrp : $("<div>", {'class': "lbox-wrp"}),
        close : $("<div>", {'class': "lbox-close"}),
        contain : $("<div>", {'class': "lbox-contain"}),
        fluidWrp : $("<div>", {'class': "lbox-fluid fluid-video"}),
        iframe : $("<iframe>", {'class': "lbox-vidplayer"}),
      };

      $(lbox.iframe).appendTo(lbox.fluidWrp);
      $(lbox.fluidWrp).appendTo(lbox.contain);
      $(lbox.contain)
        .append(lbox.close)
        .appendTo(lbox.wrp);

      $("body")
        .append(lbox.bg)
        .append(lbox.wrp);

      $(this).loadVideo({targetID: "lbox-vidplayer"});

      setTimeout(function () {
        $('.lbox-bg').addClass('lbox-active');
      }, 100);

      $('.lbox-vidplayer').load(function() {
        $(".lbox-wrp").addClass('lbox-active');
      });

      // Remove Lightbox on Close-Btn click
      $(".lbox-wrp").on("click",function(e) {
        if(!$(e.target).is('iframe')){
          e.preventDefault();
          $('.lbox-bg, .lbox-wrp').removeClass('lbox-active')
          setTimeout(function () {
            $('.lbox-bg, .lbox-wrp').remove();
          }, 400);
        }
      });

    });

  });

}());


var MediaCenter = (function(){
  $(document).ready(function(){

    // Init Responsive Tables
    $('.key-facts').stackTableRows();

    // Init select input in L3 Tabs according to screen size
    if( $(window).width() > 640) {
      $('.chosen-year-select').chosen({width:'73%'});
    } else {
      $('.chosen-year-select').chosen({width:'50%'});
    }
    // Check on Resize if Select input's need to change
    $(window).on('resize',function(){
       if( $(window).width() > 640) {
        $('.chosen-container','.year-select-wrapper').css({width:'73%'});
      } else {
        $('.chosen-container','.year-select-wrapper').css({width:'50%'});
      }
    })

    // Init Spokes People.
    $('.speaker-box').hide();

    // Spokes People Open Speaker
    $('.speaker-list-item').on('click',function(){
      $('.speaker-list').fadeOut(function(){
        $('.speaker-box').fadeIn();
      });
    });
    // Spokes People Close Speaker
    $('.close-speaker-box').on('click',function(){
      $('.speaker-box').fadeOut(function(){
        $('.speaker-list').fadeIn();
      });
    });

    // Init Select Inner Tab l2 & l3
    setSelectedInnerTab();
    // On L2 Tab Change Init it's inner L3 Tabs.
    $('li','.inner-tabs-l2-nav').on('click',setSelectedInnerTab);

  });

  function setSelectedInnerTab(){
    var NavItems =  $(".inner-tabs-l3-nav",".inner-l2-tabs-container .resp-tab-content-active").find("a");
    var Tabs = $("[class^=inner-l3]",".inner-l2-tabs-container .resp-tab-content-active .inner-l3-tabs-container");
    NavItems.removeClass('active');
    // Tabs L3 Nav init to first Item
    NavItems.first().addClass('active');
     // Tabs L3 Tab init to first Tab
    Tabs.first().addClass('active');
    Tabs.not(".active").hide();

    // L3 Nav Item Click Behaviour
    NavItems.on('click',function(e){
      e.preventDefault();
      var itemIdx = NavItems.index(this);
      // Remove current active link:
      NavItems.removeClass('active');
      // Activate clicked link:
      $(this).addClass('active');
      // Remove all active tabs:
      Tabs.removeClass('active');
      // Activate relevant Tab
      Tabs.eq(itemIdx).addClass('active').show();
      Tabs.not(".active").hide();

      $('.l3NavSelect').eq(itemIdx).val((itemIdx+1).toString())
    });}

    //Mobile Inner L3 Tab Navigation
    $('.mobile-l3-tab').first().addClass('active');
    $('.mobile-l3-tab').eq(3).addClass('active');
    $('.l3NavSelect').on('change',function(){
      // All L3 inner tabs
      var allTabs = $(this).closest('.mobile-l3-tab').siblings().addBack();
      // Tab containing the changed select input
      var tabObj = $(this).closest('.mobile-l3-tab');
      // index of the tab containing the changed select input
      var tabIdx = allTabs.index(tabObj);
      // Value of the new tab to show
      var tabToShowIdx = parseInt($(this).val())-1;
      // Tab to Show Obj;
      var tabToShowObj  = allTabs.eq(tabToShowIdx);
      // remove the active class from the tab
      tabObj.removeClass('active').hide();
      tabToShowObj.addClass('active').show();
      tabToShowObj.find('.l3NavSelect').val((tabToShowIdx+1).toString());

      $(tabObj).parent().siblings('.inner-tabs-l3-nav').find('a').removeClass('active').eq(tabToShowIdx).addClass('active');

    });
}());
