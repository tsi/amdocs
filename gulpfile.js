var gulp = require('gulp');
var gutil = require('gulp-util');
var fileinclude = require('gulp-file-include');
var paths = {
  pages: ['src/*', 'src/**/*']
};

gulp.task('fileinclude', function() {

  gulp.src(['src/*.html'])
    .pipe(fileinclude({
      prefix: '@@',
      basepath: '@file'
    })
    .on('error', gutil.log))
    .pipe(gulp.dest('./'));

  gulp.src(['src/pages/*.html'])
    .pipe(fileinclude({
      prefix: '@@',
      basepath: '@file'
    })
    .on('error', gutil.log))
    .pipe(gulp.dest('./pages/'));

});

// Rerun the task when a file changes
gulp.task('watch', function() {
  gulp.watch(paths.pages, ['fileinclude'])
});


gulp.task('default', ['fileinclude','watch']);
